FROM ubuntu:latest

ARG NGINX_VERSION

ENV NGINX_VERSION=$NGINX_VERSION

RUN apt-get update \
    && apt-get install -y curl gpg build-essential libssl-dev \
    && rm -rf /var/lib/apt/lists/*


RUN curl -O https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz \
        && curl -O https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz.asc

RUN curl -O https://nginx.org/keys/mdounin.key \
                 && apt-key add ./mdounin.key \
                 && gpg --import ./mdounin.key

RUN gpg --verify nginx-${NGINX_VERSION}.tar.gz.asc

RUN tar -zxvf nginx-${NGINX_VERSION}.tar.gz \
        && chmod +x -R nginx-${NGINX_VERSION}

COPY custom_configure.sh nginx-${NGINX_VERSION}/custom_configure.sh

RUN cd nginx-${NGINX_VERSION} \
        && chmod +x custom_configure.sh \
        && bash custom_configure.sh

WORKDIR nginx-${NGINX_VERSION}

RUN mkdir /etc/nginx /var/log/nginx /var/cache/nginx \
        && make build install \
        && chmod +rwx -R /etc/nginx

COPY nginx.conf /etc/nginx/nginx.conf

RUN chmod +rwx /etc/nginx/nginx.conf

RUN rm -rf /nginx-${NGINX_VERSION}

RUN useradd --no-create-home nginx

RUN nginx -t 

COPY entrypoint.sh ./

RUN chmod +x entrypoint.sh

CMD ["bash", "entrypoint.sh"]
